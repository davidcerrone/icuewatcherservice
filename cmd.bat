:: Create a Windows Service
sc create iCueWatcherService DisplayName="iCUE Watcher Service" binPath="E:\icuewatcherservice\src\iCueWatcherService\iCueWatcherService\bin\Release\netcoreapp3.1\publish\iCueWatcherService.exe"

:: Start a Windows Service
sc start iCueWatcherService

:: Stop a Windows Service
sc stop iCueWatcherService

:: Delete a Windows Service
sc delete iCueWatcherService