# iCueWatcherService

Restarts the iCue application after it crashes (issue when launching games like siege).

The application is written as a .NET Core Service.

# Install

`cmd.bat` lists the different command line operations to create, start, stop, and delete a service. These must be ran with Admin priviledges.

```
:: Create a Windows Service
sc create iCueWatcherService DisplayName="iCUE Watcher Service" binPath="C:\full\path\to\Demo.exe"

:: Start a Windows Service
sc start iCueWatcherService

:: Stop a Windows Service
sc stop iCueWatcherService

:: Delete a Windows Service
sc delete iCueWatcherService
```
