using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace iCueWatcherService
{
    public class Worker : BackgroundService
    {
        private const string _iCuePath = @"C:\Program Files (x86)\Corsair\CORSAIR iCUE Software\iCUE.exe";
        private readonly ILogger<Worker> _logger;

        public Worker(ILogger<Worker> logger)
        {
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                await CheckIfAppIsRunningAndRestartAsync(stoppingToken);

                await Task.Delay(5000, stoppingToken);
            }
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Starting Service");

            return base.StartAsync(cancellationToken);
        }

        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Stopping Service");

            await base.StopAsync(cancellationToken);
        }

        public override void Dispose()
        {
            _logger.LogInformation("Disposing Service");

            base.Dispose();
        }

        private async Task CheckIfAppIsRunningAndRestartAsync(CancellationToken stoppingToken)
        {
            if (Process.GetProcessesByName("iCUE").Length > 0)
            {
                // App is running
                _logger.LogInformation("App running at: {time}", DateTimeOffset.Now);
            }
            else
            {
                // App not running
                _logger.LogInformation("Restarting iCUE");

                // Start the process
                Process.Start(_iCuePath);

                // Wait while the app boots
                await Task.Delay(TimeSpan.FromSeconds(10), stoppingToken);
            }
        }
    }
}
